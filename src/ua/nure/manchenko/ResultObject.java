package ua.nure.manchenko;

import java.io.File;

/**
 * Created by stain on 02.10.17.
 */
public class ResultObject {
    private String path;
    private int countFiles = 0;
    private static int countPaths = 0;
    private int num;

    public ResultObject(String path)
    {
        this.path = path;
        this.num = ++countPaths;
    }

    public int getNum() {
        return num;
    }

    public String getPath() {
        return path;
    }

    public int getCountFiles() {
        return countFiles;
    }


    public void checkCountOfFiles(File Path)
    {
        File[] folderEntries = Path.listFiles();
        for (File entry : folderEntries)
        {
            if (entry.isDirectory())
            {
                checkCountOfFiles(entry);
            }
            countFiles++;
        }
    }
}
