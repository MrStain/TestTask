package ua.nure.manchenko;

import javenue.csv.Csv;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main extends Thread{
    String input;
    String output;
    public Main(String a, String b)
    {
        this.input = a;
        this.output = b;
    }
    @Override
    public void run()
    {
        List<ChildThread> threads = new ArrayList<>(); //контейнер для хранения дочерних потоков
        try {
            BufferedReader reader = new BufferedReader(new FileReader(input));//Открываем файл для чтения
            String pass;
            ArrayList<ResultObject> result = new ArrayList<>();
            while ((pass = reader.readLine()) != null) {
                result.add(new ResultObject(pass));
            }
            if (result.isEmpty()) {
                System.out.println("Input file is Empty");
            }
            else {
                for (int i = 0; i < result.size(); i++) {
                    ChildThread thread = new ChildThread(result.get(i));
                    thread.start();
                    threads.add(thread);
                }
                GlobalScreen.registerNativeHook();//открываем возможность регистрации хука
                ESCHook escHook = new ESCHook();//создание обьекта нового хука
                GlobalScreen.addNativeKeyListener(escHook);//регистрация хука
                boolean allEnd = false;
                do
                {
                    for(ChildThread ct : threads)//проверяем завершился ли поток
                        /*
                        В случае если бы не было условия с выходом по нажатию клавиши, данная часть кода многократно сократилась бы
                        в
                        isInterrupted
                         */
                    {
                        if(!ct.isEnd)
                        {
                            allEnd = false;
                            break;
                        }
                        else allEnd = true;
                    }
                }
                while (!allEnd||!escHook.ESC);
                endAllChildThreads(threads);//завершаем все потоки
                resultSetToCSVFile(result, output);//сохраняем в файл(создается новый файл)
                resultSetToTerminal(result);//вывод результата на экран
                GlobalScreen.unregisterNativeHook();//убираем зарегестрированые хуки
            }
        }
        catch(FileNotFoundException e)//при отсутсвии файла выбросим соответсвующего сообщения
        {
            System.out.println("Input file not found");
        }
        catch (IOException io)//исключение в основном будет при неправильной кодировке в файле
        {
            System.out.println("Encoding problems");
        }
        catch(NativeHookException nhe)//проблема при установке хука
        {
            System.out.println("Some trouble");
        }
    }
    public static void main(String[] args) {
        {
            Main main;
            if (args.length == 0)
                System.out.println("Enter args please");
            else {
                if (args[0] == "" || args[0] == null)
                    System.out.println("You forgot wright input filename");
                else if (args[1] == "" || args[1] == null)
                    System.out.println("You forgot wright output filename");
                else {
                    File file = new File(args[1]);
                    if (!file.exists()) {
                        main = new Main(args[0], args[1]);
                        main.start();//
                    }
                    else
                        System.out.println("Output file already exists");
                }
            }
        }
    }

    private void resultSetToTerminal(ArrayList<ResultObject> res)
    {
        for(ResultObject obj : res)
        {
            System.out.println(obj.getNum() + "\t" + obj.getCountFiles() +  "\t" + obj.getPath() + System.lineSeparator());
        }
    }
    private void resultSetToCSVFile(ArrayList<ResultObject> res, String output)
    {
        Csv.Writer writer = new Csv.Writer(output).delimiter(';');
        for(ResultObject obj : res) {
            writer.value(obj.getPath()).value((Integer.toString(obj.getCountFiles()))).newLine();
        }
        writer.close();
    }

    private void endAllChildThreads(List<ChildThread> childThreadList)
    {
        for(ChildThread ct: childThreadList)
            if(!ct.isInterrupted())
            ct.interrupt();
    }
}
