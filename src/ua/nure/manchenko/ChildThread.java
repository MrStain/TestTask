package ua.nure.manchenko;

import java.io.File;

/**
 * Created by stain on 03.10.17.
 */
public class ChildThread extends Thread {
    private ResultObject ro;
    public boolean isEnd = false;
    public ChildThread(ResultObject ro){
        this.ro = ro;
        start();
    }

    @Override
    public void run() {
        try
        {
            File file = new File(ro.getPath());
            if(!file.exists())
                throw new NoSuchDirectoryException("No such directory");
            ro.checkCountOfFiles(file);
            isEnd = true;//
        }
        catch (NoSuchDirectoryException e)
        {
            System.out.println("No such file" + ro.getPath());
            isEnd = true;
            interrupt();//
        }
    }
    private class NoSuchDirectoryException extends Exception
    {
        NoSuchDirectoryException(String msg)
        {
            super(msg);
        }
    }
}
