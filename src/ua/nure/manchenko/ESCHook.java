package ua.nure.manchenko;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/**
 * Created by stain on 03.10.17.
 */
public class ESCHook implements NativeKeyListener{
    boolean ESC = false;
    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {//хук устанавливается в фоне, т.е. даже при свёрнутой программе
        //будет реакция на нажатие клавиши
        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_ESCAPE)
        ESC = true;
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }
}
